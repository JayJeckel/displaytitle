# Display Title Dokuwiki Plugin

[![MIT License](https://svgshare.com/i/TRb.svg)](https://opensource.org/licenses/MIT)
[![DokuWiki Plugin](https://svgshare.com/i/TSa.svg)](https://www.dokuwiki.org/dokuwiki)
[![Plugin Home](https://svgshare.com/i/TRw.svg)](https://www.dokuwiki.org/plugin:displaytitle)
[![Gitlab Repo](https://svgshare.com/i/TRR.svg)](https://gitlab.com/JayJeckel/displaytitle)
[![Gitlab Issues](https://svgshare.com/i/TSw.svg)](https://gitlab.com/JayJeckel/displaytitle/issues)
[![Gitlab Download](https://svgshare.com/i/TT5.svg)](https://gitlab.com/JayJeckel/displaytitle/-/archive/master/displaytitle-master.zip)

The Display Title Plugin can display the id or title of the current page or the title of an arbitrary page.

## Installation

Search and install the plugin using the [Extension Manager](https://www.dokuwiki.org/plugin:extension) or install directly using the latest [download url](https://gitlab.com/JayJeckel/displaytitle/-/archive/master/displaytitle-master.zip), otherwise refer to [Plugins](https://www.dokuwiki.org/plugins) on how to install plugins manually.

## Usage

The plugin offers three inline elements that expand into the text of either a page id or page title.

| Element | Replaced By |
|:-|:-|
| `<<display id>>` | The id of the current page. |
| `<<display title>>` | The title of the current page. |
| `<<display title ID>>` | The title of the page `ID` or current page if `ID` is empty. |

| Argument | Required | Description |
|:-|:-|:-|
| `ID` | no | Arbitrary local wiki page id as would be passed to normal local wiki links. |

## Configuration Settings

The plugin does not provide any [Configuration Manager](https://www.dokuwiki.org/config:manager) settings.

## Security

The plugin has no abnormal security concerns related to the provided functionality.
